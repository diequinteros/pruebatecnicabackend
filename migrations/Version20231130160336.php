<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231130160336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cliente (id INT AUTO_INCREMENT NOT NULL, municipio_id INT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, direccion VARCHAR(255) NOT NULL, INDEX IDX_F41C9B2558BC1BE0 (municipio_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departamento (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detalle (id INT AUTO_INCREMENT NOT NULL, producto_id INT NOT NULL, orden_id INT NOT NULL, cantidad INT NOT NULL, INDEX IDX_80397C307645698E (producto_id), INDEX IDX_80397C309750851F (orden_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE municipio (id INT AUTO_INCREMENT NOT NULL, departamento_id INT NOT NULL, nombre VARCHAR(255) NOT NULL, INDEX IDX_FE98F5E05A91C08D (departamento_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orden (id INT AUTO_INCREMENT NOT NULL, cliente_id INT NOT NULL, fecha_hora DATETIME NOT NULL, INDEX IDX_E128CFD7DE734E51 (cliente_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE producto (id INT AUTO_INCREMENT NOT NULL, unidad_medida_id INT NOT NULL, nombre VARCHAR(255) NOT NULL, precio DOUBLE PRECISION NOT NULL, descripcion VARCHAR(255) NOT NULL, INDEX IDX_A7BB06152E003F4 (unidad_medida_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE unidad_medida (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, descripcion VARCHAR(255) NOT NULL, abreviacion VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cliente ADD CONSTRAINT FK_F41C9B2558BC1BE0 FOREIGN KEY (municipio_id) REFERENCES municipio (id)');
        $this->addSql('ALTER TABLE detalle ADD CONSTRAINT FK_80397C307645698E FOREIGN KEY (producto_id) REFERENCES producto (id)');
        $this->addSql('ALTER TABLE detalle ADD CONSTRAINT FK_80397C309750851F FOREIGN KEY (orden_id) REFERENCES orden (id)');
        $this->addSql('ALTER TABLE municipio ADD CONSTRAINT FK_FE98F5E05A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('ALTER TABLE orden ADD CONSTRAINT FK_E128CFD7DE734E51 FOREIGN KEY (cliente_id) REFERENCES cliente (id)');
        $this->addSql('ALTER TABLE producto ADD CONSTRAINT FK_A7BB06152E003F4 FOREIGN KEY (unidad_medida_id) REFERENCES unidad_medida (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cliente DROP FOREIGN KEY FK_F41C9B2558BC1BE0');
        $this->addSql('ALTER TABLE detalle DROP FOREIGN KEY FK_80397C307645698E');
        $this->addSql('ALTER TABLE detalle DROP FOREIGN KEY FK_80397C309750851F');
        $this->addSql('ALTER TABLE municipio DROP FOREIGN KEY FK_FE98F5E05A91C08D');
        $this->addSql('ALTER TABLE orden DROP FOREIGN KEY FK_E128CFD7DE734E51');
        $this->addSql('ALTER TABLE producto DROP FOREIGN KEY FK_A7BB06152E003F4');
        $this->addSql('DROP TABLE cliente');
        $this->addSql('DROP TABLE departamento');
        $this->addSql('DROP TABLE detalle');
        $this->addSql('DROP TABLE municipio');
        $this->addSql('DROP TABLE orden');
        $this->addSql('DROP TABLE producto');
        $this->addSql('DROP TABLE unidad_medida');
    }
}
