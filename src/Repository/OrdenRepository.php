<?php

namespace App\Repository;

use App\Entity\Orden;
use App\Service\Pagination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Orden>
 *
 * @method Orden|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orden|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orden[]    findAll()
 * @method Orden[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdenRepository extends ServiceEntityRepository
{
    private Pagination $pagination;
    public function __construct(ManagerRegistry $registry, Pagination $pagination)
    {
        parent::__construct($registry, Orden::class);
        $this->pagination = $pagination;
    }

    public function findAllWithPagination(int $currentPage, int $limit): Paginator
    {
      // Creamos nuestra query
      $queryBuilder = $this->createQueryBuilder('p');

      $query = $queryBuilder->getQuery();

      // Creamos un paginator con la funcion paginate
      $paginator = $this->pagination->paginate($query, $currentPage, $limit);

      return $paginator;
    }

//    /**
//     * @return Orden[] Returns an array of Orden objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('o.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Orden
//    {
//        return $this->createQueryBuilder('o')
//            ->andWhere('o.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
