<?php

namespace App\Controller;

use App\Entity\Cliente;
use App\Service\GeneradorDeMensajes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cliente', name: 'app_cliente')]
class ClienteController extends AbstractController
{
    #[Route('', name: 'app_cliente_read_by_nombre_apellido', methods: ['GET'])]
    public function readByNombreApellido(Request $request, EntityManagerInterface $entityManager, GeneradorDeMensajes $generadorDeMensajes): JsonResponse
    {	
		$value = $request->get('value', '');

		$clientes = $entityManager->getRepository(Cliente::class)->findByNombreApellido($value);

		$clientesData = [];
		foreach($clientes as $cliente){
			$clienteData = [
				'id' => $cliente->getId(),
				'nombre' => $cliente->getNombre(),
				'apellido' => $cliente->getApellido()
			];

			$clientesData[] = $clienteData;
		}

        return $this->json($generadorDeMensajes->generarRespuesta('Solicitud procesada con exito.', $clientesData) );
    }
}
