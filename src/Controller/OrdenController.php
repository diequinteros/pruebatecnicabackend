<?php

namespace App\Controller;

use App\Entity\Cliente;
use App\Entity\Detalle;
use App\Entity\Orden;
use App\Entity\Producto;
use App\Service\GeneradorDeMensajes;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/orden', name: 'app_orden')]
class OrdenController extends AbstractController
{
	private function serializeOrden($orden){
		$clienteOrden = $orden->getCliente();
		$clienteData = [
			"id" => $clienteOrden->getId(),
			"nombre" => $clienteOrden->getNombre(),
			"apellido" => $clienteOrden->getApellido(),
			"direccion" => $clienteOrden->getDireccion(),
			"municipio" => $clienteOrden->getMunicipio()->getNombre(),
			"departamento" => $clienteOrden->getMunicipio()->getDepartamento()->getNombre()
		];

		$detallesOrden = $orden->getDetalles();
		$detallesData = [];

		foreach($detallesOrden as $detalle){
			$detallesData[] = [
				"id" => $detalle->getId(),
				"producto" => [
          "id" => $detalle->getProducto()->getId(),
					"nombre" => $detalle->getProducto()->getNombre(),
					"precio" => $detalle->getProducto()->getPrecio(),
					"descripcion" => $detalle->getProducto()->getDescripcion(),
					"unidadMedida" => $detalle->getProducto()->getUnidadMedida()->getAbreviacion()
				],
				"cantidad" => $detalle->getCantidad()
			];
		}

		return [
			'id' => $orden->getId(),
			'fechaHora' => $orden->getFechaHora(),
			'cliente' => $clienteData,
			'detalles' => $detallesData
		];
	}

    #[Route('', name: 'app_orden_create', methods:['POST'])]
    public function create(EntityManagerInterface $entityManager, GeneradorDeMensajes $generadorDeMensajes, Request $request): JsonResponse
    {
        $orden = new Orden();
        $orden->setFechaHora(new DateTime("now", new DateTimeZone("America/El_Salvador")));

        $clienteId = $request->get('cliente');
        $cliente = $entityManager->getRepository(Cliente::class)->find($clienteId);
        if($cliente === null){
          return $this->json("No existe un cliente con id ".$clienteId.".", 404);	
        }
        $orden->setCliente($cliente);
        $productos = $request->get('productos');

        foreach($productos as $productoJSON){
          $nuevoDetalle = new Detalle();
          $producto = $entityManager->getRepository(Producto::class)->find($productoJSON['id']);
          if($producto === null){
            return $this->json("No existe un producto con id ".$productoJSON['id'].".", 404);	
          }
          $nuevoDetalle->setProducto($producto);
          $nuevoDetalle->setCantidad($productoJSON['cantidad']);
          $entityManager->persist($nuevoDetalle);	
          $orden->addDetalle($nuevoDetalle);
        }

        $entityManager->persist($orden);
        $entityManager->flush();

        return $this->json($generadorDeMensajes->generarRespuesta("Se registro la orden con exito."));
    }

	#[Route('', name: 'app_orden_read_all', methods: ['GET'])]
	public function readAll(EntityManagerInterface $entityManager, Request $request, GeneradorDeMensajes $generadorDeMensajes): JsonResponse
	{
		$limit = $request->get('limit', 15);
		$page = $request->get('page', 1);

		$ordenes = $entityManager->getRepository(Orden::class)->findAllWithPagination($page, $limit);
		$total = $ordenes->count();
		$totalPages = (int) ceil($total/$limit);

		$data = [];
	
		foreach ($ordenes as $orden) {
			$data[] = $this->serializeOrden($orden);
		}
		
		return $this->json($generadorDeMensajes->generarRespuesta('Se proceso la solicitud con exito.', ['totalPages' => $totalPages, 'total' => $total, 'ordenes' => $data]) ); 
	}

	#[Route('/{id}', name: 'app_orden_read_one', methods: ['GET'])]
	public function readOne(int $id, EntityManagerInterface $entityManager, Request $request, GeneradorDeMensajes $generadorDeMensajes): JsonResponse
	{
		$orden = $entityManager->getRepository(Orden::class)->find($id);

		if($orden === null){
			return $this->json('No se encontro ninguna orden con id '.$id.'.', 404); 	
		}

		$data = $this->serializeOrden($orden);
		
		return $this->json($generadorDeMensajes->generarRespuesta('Se proceso la solicitud con exito.', $data) ); 
	}
}
