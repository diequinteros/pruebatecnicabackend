<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Service\GeneradorDeMensajes;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/producto', name: 'app_producto')]
class ProductoController extends AbstractController
{
	#[Route('', name: 'app_producto_read_id_nombre', methods: ['GET'])]
    public function readByIdNombre(Request $request, EntityManagerInterface $entityManager, GeneradorDeMensajes $generadorDeMensajes): JsonResponse
    {
        $value = $request->get('value', '');
		$ignore = $request->get('ignore', '');

        $productos = $entityManager->getRepository(Producto::class)->findByIdOrName($value, $ignore);
    
        $productosData = [];
        foreach($productos as $producto){
          $productoData = [
            'id' => $producto->getId(),
            'nombre' => $producto->getNombre(),
            'descripcion' => $producto->getDescripcion(),
            'precio' => $producto->getPrecio(),
            'unidadMedida' => $producto->getUnidadMedida()->getAbreviacion()
          ];
    
          $productosData[] = $productoData;
        }
    
        return $this->json($generadorDeMensajes->generarRespuesta('Solicitud procesada con exito.', $productosData) );
    }
}
